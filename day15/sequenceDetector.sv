module sequenceDetector#(parameter seqWidth=3)(
  input clk,rst,
  input dataIn,
  input [seqWidth-1:0] seq,
  output reg equal
);

  logic [seqWidth-1:0] holdSeq;

  always@(posedge clk or negedge rst) begin
    if(!rst) begin
      equal=1'b0;
      holdSeq='b0;
    end
    else begin
      holdSeq<={holdSeq[seqWidth-2:0],dataIn};
    end
  end

  always_comb begin
    equal=(holdSeq==seq)?1'b1:1'b0;
  end

endmodule
