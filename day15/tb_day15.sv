module tb_day15();

  localparam seqWidth=3;
  reg dataIn,clk=1'b0,rst;
  reg [seqWidth-1:0] seq;
  wire equal;

  always#5 clk=~clk;
  sequenceDetector#(seqWidth) inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    rst=1'b1;
    seq=3'b100;
    for(i=0;i<16;i=i+1) begin
      dataIn<=$urandom_range(1'b0,1'b1);
      @(posedge clk);
    end
    seq=3'b010;
    for(i=0;i<16;i=i+1) begin
      dataIn<=$urandom_range(1'b0,1'b1);
      @(posedge clk);
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day15);
    $dumpfile("dump.vcd");
  end

endmodule
