module tb_day14();

  parameter widthDataIn=3,widthDataOut=8;
  reg [widthDataIn-1:0] data_in;
  wire [widthDataOut-1:0] oneHot;

  oneHotConverter#(widthDataIn,widthDataOut) inst(.*);

  integer i;
  initial begin
    for(i=0;i<32;i=i+1)begin
      data_in=$urandom_range(3'b0,3'b111);
      #5;
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day14);
    $dumpfile("dump.vcd");
  end

endmodule
