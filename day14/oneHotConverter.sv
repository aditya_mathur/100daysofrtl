module oneHotConverter#(parameter widthDataIn=3,widthDataOut=8)(
  input [widthDataIn-1:0] data_in,
  output [widthDataOut-1:0] oneHot
);

  assign oneHot=1'b1<<data_in;


endmodule
