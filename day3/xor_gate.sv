module xor_gate(
  input a,b,
  output reg out,out1,out2
);

  //gate level modeling
  //
  wire inter1,inter2;

  not ins1 (inv_a,a);
  not ins2 (inv_b,b);

  and ins3 (inter1,a,inv_b);
  and ins4 (inter2,b,inv_a);

  or ins5 (out,inter1,inter2);

  //dataflow modeling
  //
  assign out1=(a&(~b))|((~a)&b);

  //behavioural modeling
  //
  always@(a,b) begin
    case({a,b})
      2'b00:out2=1'b0;
      2'b01:out2=1'b1;
      2'b10:out2=1'b1;
      2'b11:out2=1'b0;
      default: out2=1'b0;
    endcase
  end

endmodule
