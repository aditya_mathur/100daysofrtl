module tb_xor_day3();

  reg a,b;
  wire out,out1,out2;

  xor_gate inst(.*);

  integer i;

  initial begin
    for(i=0;i<100;i=i+1) begin
      {a,b}=$urandom_range('b00,'b11);
      #25;
    end
    #10
    $finish();
  end

  initial begin
    $dumpvars(0,tb_xor_day3);
    $dumpfile("dump.vcd");
  end

endmodule
