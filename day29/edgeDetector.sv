module edgeDetector(
  input din,
  input clk,rst,
  output logic dout
);

  logic temp;
  logic [1:0] pState,nState;


  always@(posedge clk or negedge rst) begin
    if(!rst) begin
      dout=1'b0;
      pState=2'b0;
      temp=1'b0;
    end
    else begin
      pState=nState;
      dout=temp;
    end
  end

  always@(pState or din) begin
    nState=2'bx;
    case(pState)
      2'b00: begin
        if(din==1'b1) begin
          temp=1'b1;
          nState=2'b01;
        end
        else begin
          nState=2'b00;
        end
      end
      2'b01: begin
        if(din==1'b1) begin
          nState=2'b01;
        end
        else begin
          nState=2'b00;
        end
        temp=2'b0;
      end
    endcase
  end



endmodule
