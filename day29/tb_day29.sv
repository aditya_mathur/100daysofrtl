module tb_day29();

  logic din;
  logic clk=1'b0;
  logic rst;

  wire dout;

  always#5 clk=~clk;

  edgeDetector inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    rst=1'b1;
    @(posedge clk);
    for(i=0;i<50;i=i+1) begin
      din=$random();
      #7;
    end
    #5;
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day29);
    $dumpfile("dump.vcd");
  end


endmodule
