module tb_day17();

  logic [7:0] in;
  wire [2:0] out;

  encoder inst(.*);

  integer i;
  initial begin
    for(i=0;i<32;i=i+1) begin
      in<=1'b1<<$urandom_range(0,4'hf);
      #5;
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day17);
    $dumpfile("dump.vcd");
  end

endmodule
