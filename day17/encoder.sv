module encoder(
  input[7:0] in,
  output reg [2:0] out
);

  always@(*) begin
    out[0]=in[7]+in[5]+in[3]+in[1];
    out[1]=in[7]+in[6]+in[3]+in[2];
    out[2]=in[7]+in[6]+in[5]+in[4];
  end

endmodule
