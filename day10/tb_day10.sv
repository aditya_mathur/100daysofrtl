module tb_day10();

  logic rst,clk=1'b0;
  wire [2:0] q;

  always #5 clk=~clk;

  asyncCounter_day10 inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    rst=1'b1;
    for(i=0;i<16;i=i+1) begin
      @(posedge clk);
    end
    #10
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day10);
    $dumpfile("dump.vcd");
  end
endmodule
