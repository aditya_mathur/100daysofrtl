module asyncCounter_day10(
  input clk,rst,
  output reg[2:0] q
);

  wire t=1'b1;
  wire q0ck1,q1ck2,q2;

  TFF inst(.clk(clk),.rst(rst),.t(t),.q(q0ck1));
  TFF inst1(.clk(q0ck1),.rst(rst),.t(t),.q(q1ck2));
  TFF inst2(.clk(q1ck2),.rst(rst),.t(t),.q(q2));

  assign q={q2,q1ck2,q0ck1};

endmodule


module TFF(
  input rst,clk,t,
  output reg q
);

  always@(posedge clk or negedge rst) begin
    if(!rst) begin
      q<=1'b0;
    end
    else begin
      if(t) q<=~q;
      else q<=q;
    end
  end

endmodule
