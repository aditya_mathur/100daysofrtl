module dualPortMem#(parameter DWIDTH=8,AWIDTH=8)(
  input clk,rst,
  input w_a_en_i,w_b_en_i,
  input r_a_en_i,r_b_en_i,
  input [AWIDTH-1:0] w_a_addr_i, w_b_addr_i,
  input [DWIDTH-1:0] w_a_data_i, w_b_data_i,
  output logic [DWIDTH-1:0] r_a_data_o,r_b_data_o,
  output valid_a,valid_b
);

 dualPortMem_core#(.AWIDTH(AWIDTH),.DWIDTH(DWIDTH)) inst_memCore (.*);

  assign valid_a = (w_a_en_i&&r_a_en_i)?1'b0:1'b1;
  assign valid_b = (w_b_en_i&&r_b_en_i)?1'b0:1'b1;


endmodule

//mem core
module dualPortMem_core#(parameter DWIDTH=8,AWIDTH=8)(
  input logic clk,rst,
  input logic w_a_en_i,w_b_en_i,
  input logic r_a_en_i,r_b_en_i,
  input logic [AWIDTH-1:0] w_a_addr_i, w_b_addr_i,
  input logic [DWIDTH-1:0] w_a_data_i, w_b_data_i,
  output logic [DWIDTH-1:0] r_a_data_o,r_b_data_o
);

  logic [DWIDTH-1:0] mem [2**AWIDTH-1:0];

  integer i;
  always@(posedge clk or negedge rst) begin
    if(!rst) begin
      for(i=0;i<2**AWIDTH;i=i+1) begin
        mem[i]='b0;
      end
    end
    else begin
      if(w_a_en_i) mem[w_a_addr_i]<=w_a_data_i;
      if(w_b_en_i) mem[w_b_addr_i]<=w_b_data_i;
    end
  end

  always_comb begin
    if(r_a_en_i) r_a_data_o=mem[w_a_addr_i];
    if(r_b_en_i) r_b_data_o=mem[w_b_addr_i];
  end

endmodule
