module tb_day27();

  localparam AWIDTH=4,DWIDTH=4;
  logic clk=1'b0,rst=1'b0;

  logic w_a_en_i,w_b_en_i;
  logic r_a_en_i,r_b_en_i;
  logic [AWIDTH-1:0] w_a_addr_i;
  logic [AWIDTH-1:0] w_b_addr_i;
  logic [DWIDTH-1:0] w_a_data_i;
  logic [DWIDTH-1:0] w_b_data_i;
  wire [DWIDTH-1:0] r_a_data_o,r_b_data_o;
  wire valid_a,valid_b;


  always#5 clk=~clk;

  dualPortMem#(.AWIDTH(AWIDTH),.DWIDTH(DWIDTH)) inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    @(posedge clk);
    rst=1'b1;
    //Write cycle
    {r_a_en_i,r_b_en_i}=2'b00;
    {w_a_en_i,w_b_en_i}=2'b11;
    for(i=0;i<2**AWIDTH;i=i+1) begin
      w_a_addr_i=$urandom_range(4'b0,4'b0111);
      w_b_addr_i=$urandom_range(4'b1000,4'b1111);
      w_a_data_i=$random();
      w_b_data_i=$random();
      @(posedge clk);
    end
    //Read Cycle
    {w_a_en_i,w_b_en_i}=2'b00;
    @(posedge clk);
    @(posedge clk);
    {r_a_en_i,r_b_en_i}=2'b11;
    for(i=0;i<2**AWIDTH;i=i+1) begin
      w_b_addr_i=$urandom_range(4'b0,4'b0111);
      w_a_addr_i=$urandom_range(4'b1000,4'b1111);
      @(posedge clk);
    end

    #10;
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day27);
    $dumpfile("dump.vcd");
  end

endmodule
