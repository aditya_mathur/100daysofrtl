module tb_day16();

  localparam inputWidth=4;

  logic [inputWidth-1:0] paraIn;
  logic clk=1'b0,rst;
  wire serialOut,ready;
  wire outValid;

  always#5 clk=~clk;

  parallel2Serial#(inputWidth) inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    rst=1'b1;
    for(i=0;i<128;i=i+1) begin
      if(ready==1'b1)paraIn=$urandom_range(4'h0,4'hf);
      @(posedge clk);
    end
    #10
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day16);
    $dumpfile("dump.vcd");
  end


endmodule
