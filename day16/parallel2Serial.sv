module parallel2Serial#(parameter inputWidth=4 )(
  input clk,rst,
  input [inputWidth-1:0] paraIn,
  output serialOut, ready,
  output outValid
);

  //ready==1 -> shift reg is empty
  //ready==0 -> shift reg is not empty
  //
  //shift register
  logic [inputWidth-1:0] pre_reg,nxt_reg;

  always@(posedge clk or negedge rst) begin
    if(!rst) pre_reg<='b0;
    else begin
      pre_reg<=nxt_reg;
    end
  end

  assign nxt_reg=(ready)?paraIn:({1'b0,pre_reg[inputWidth-1:1]});
  assign serialOut=pre_reg[0];


  //counter to toggle ready and out valid
  //counter =0 -> ready==1
  //counter >0 -> output valid
  //
  logic [1:0] counter;

  always@(posedge clk or negedge rst) begin
    if(!rst) counter<='b0;
    else counter<=counter+1'b1;
  end

  assign ready=(counter==3'b0)?1'b1:1'b0;
  assign outValid=(counter>3'b0)?1'b1:1'b0;

endmodule
