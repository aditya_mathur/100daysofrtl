module clk_divider(
  input data_in,
  input clk,rst,
  input [1:0] div_rate,
  output logic data_out
);

  logic [1:0] pState,nState;

  always@(posedge clk or negedge rst) begin
    if(!rst) begin
      pState<=2'b0;
      data_out<=1'b0;
    end
    else begin
      pState<=nState;
    end
  end

  always@(pState) begin
    nState=2'bxx;
    case(pState)
      2'b00:nState=2'b01;
      2'b01:begin
        if (div_rate==2'b10) nState=2'b00;
        else nState=2'b10;
      end
      2'b10:
        nState=2'b11;
      2'b11:
        nState=2'b00;
    endcase

  end

    always_comb begin
      if(div_rate==2'b10 && pState==2'b01) data_out=~data_out;
      else if(div_rate==2'b11 && pState==2'b11) data_out=~data_out;
    end


endmodule
