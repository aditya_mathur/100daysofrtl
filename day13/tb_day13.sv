module tb_day13();

  logic data_in,clk=1'b0,rst;
  logic [1:0]div_rate;
  wire data_out;

  always #5 clk=~clk;

  clk_divider inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    data_in=1'b1;
    @(posedge clk);
    @(posedge clk);
    rst=1'b1;
    for(i=0;i<16;i=i+1) begin
      div_rate=2'b10;
      data_in<=~data_in;
      @(posedge clk);
    end
    for(i=0;i<16;i=i+1) begin
      div_rate=2'b11;
      data_in<=~data_in;
      @(posedge clk);
    end
   #10
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day13);
    $dumpfile("dump.vcd");
  end


endmodule
