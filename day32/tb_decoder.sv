`include "testCore_defines.sv"

import testCore_defines::*;

module tb_decode();

  localparam XLEN=32;

  logic [XLEN-1:0] inst;
  logic [XLEN-1:0] PC;

  //Exec unit related output
  wire [ALU_opcodeWidth-1:0] ALU_opcode_o;
  wire [ALU_opcodeWidth-1:0] ALU_rs1_o;
  wire [ALU_opcodeWidth-1:0] ALU_rs2_o;
  wire [ALU_opcodeWidth-1:0] ALU_rd_o;

  //Control unit/core FSM related outputs
  wire CNT_isBranch;
  wire CNT_isJump;
  wire CNT_isALU;
  wire CNT_isLoad;
  wire CNT_isStore;
  wire CNT_isImmediate;


  logic [4:0] opcode;
  logic [4:0] rs1;
  logic [4:0] rs2;
  logic [4:0] rd;
  logic [2:0] op1;
  logic [6:0] op2;

  decoder#(.XLEN(XLEN)) U_decoder(.*);

  integer i;
  initial begin
    PC='b0;
    for(i=0;i<100;i=i+1) begin
      opcode=$urandom_range(5'b00000,5'b01100);
      rs1=$urandom_range(5'b0,5'b00111);
      rs2=$urandom_range(5'b01000,5'b10000);
      rd=$urandom_range(5'b11100,5'b11111);
      op1=$urandom_range(3'b0,3'b111);
      op2[6:5]=$urandom_range(2'b0,2'b01);
      op2[4:0]=5'b0;

      inst={op2,rs2,rs1,op1,rd,opcode,2'b0};

      #5;
    end

    #5
    $finish();
  end


  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,tb_decoder);
  end



endmodule
