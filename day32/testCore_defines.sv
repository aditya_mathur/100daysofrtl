package testCore_defines;

  //add(00 000)
  //sub(01 000)
  //sll(00 001) -> shift left logical
  //slt(00 010) -> signed compare rs1,rs2 if rs1<rs2 write 1 else 0
  //sltu(00 011) ->unsigned compare rs1,rs2 if rs1<rs2 write 1 else 0
  //xor(00 100) ->
  //srl(00 101) -> shift right logical
  //sra(01 101) -> shift right arithmetic
  //or(00 110)
  //and(00 111)

  parameter ALU_opcodeWidth = 5;
  parameter ALU_ADD = 5'b00_000;
  parameter ALU_SUB = 5'b01_000;
  parameter ALU_SLL = 5'b00_001;
  parameter ALU_SLTU = 5'b00_011;
  parameter ALU_XOR = 5'b00_100;
  parameter ALU_SRL = 5'b00_101;
  parameter ALU_SRA = 5'b01_101;
  parameter ALU_OR = 5'b00_110;
  parameter ALU_AND = 5'b00_111;
  parameter ALU_SLT = 5'b00_010;

endpackage
