//`include "testCore_defines.sv"

//import testCore_defines::*;

module decoder#(parameter XLEN=32)(

  parameter ALU_opcodeWidth = 5;
  parameter ALU_ADD = 5'b00_000;
  parameter ALU_SUB = 5'b01_000;
  parameter ALU_SLL = 5'b00_001;
  parameter ALU_SLTU = 5'b00_011;
  parameter ALU_XOR = 5'b00_100;
  parameter ALU_SRL = 5'b00_101;
  parameter ALU_SRA = 5'b01_101;
  parameter ALU_OR = 5'b00_110;
  parameter ALU_AND = 5'b00_111;
  parameter ALU_SLT = 5'b00_010;


  //inputs from Fetch unit
  input logic [XLEN-1:0] inst,
  input logic [XLEN-1:0] PC,

  //Exec unit related output
  output logic [ALU_opcodeWidth-1:0] ALU_opcode_o,
  output logic [ALU_opcodeWidth-1:0] ALU_rs1_o,
  output logic [ALU_opcodeWidth-1:0] ALU_rs2_o,
  output logic [ALU_opcodeWidth-1:0] ALU_rd_o,

  //Control unit/core FSM related outputs
  output logic CNT_isBranch,
  output logic CNT_isJump,
  output logic CNT_isALU,
  output logic CNT_isLoad,
  output logic CNT_isStore,
  output logic CNT_isImmediate
);

  // type of instruction supported will be as follows:
  // 1. alu-register type(opcode 01100) -> operation will be performed b/w 2 registers
  // 2. alu-immediate type(opcode 00100) => operation will be performed b/w 1 register and immediate value
  // 3. load type (opcode 00000)
  // 4. store type (opcode 01001)-> store instruction
  // 5. branch type (opcode 01000)-> branch instruction

  //instruction supported:
  //add(00 000)
  //sub(01 000)
  //sll(00 001) -> shift left logical
  //slt(00 010) -> signed compare rs1,rs2 if rs1<rs2 write 1 else 0
  //sltu(00 011) ->unsigned compare rs1,rs2 if rs1<rs2 write 1 else 0
  //xor(00 100) ->
  //srl(00 101) -> shift right logical
  //sra(01 101) -> shift right arithmetic
  //or(00 110)
  //and(00 111)

  logic [4:0] inst_opcode;
  logic [4:0] inst_rs1;
  logic [4:0] inst_rs2;
  logic [4:0] inst_rd;
  logic [2:0] inst_op1;
  logic [6:0] inst_op2;

  always_comb
  begin

    //extracting the default values from the instruction received
    //
    inst_opcode = inst[6:2];
    inst_rs1 = inst[19:15];
    inst_rs2 = inst[24:20];
    inst_rd = inst[11:7];
    inst_op1 = inst[14:12];
    inst_op2 = inst[31:25];

    CNT_isALU = (inst_opcode==5'b01100);
    CNT_isLoad = (inst_opcode==5'b00000);
    CNT_isStore = (inst_opcode==5'b01001);
    CNT_isBranch = (inst_opcode==5'b01000);
    CNT_isImmediate = (inst_opcode==5'b00100);

    case(inst_opcode)
      5'b01_100: begin
        case({inst_op2[6:5],inst_op1})
          ALU_ADD: ALU_opcode_o=ALU_ADD;
          ALU_SUB: ALU_opcode_o=ALU_SUB;
          ALU_OR: ALU_opcode_o=ALU_OR;
          ALU_SLL: ALU_opcode_o=ALU_SLL;
          ALU_SRA: ALU_opcode_o=ALU_SRA;
          ALU_SRL: ALU_opcode_o=ALU_SRL;
          ALU_SLTU: ALU_opcode_o=ALU_SLTU;
          ALU_XOR: ALU_opcode_o=ALU_XOR;
          ALU_AND: ALU_opcode_o=ALU_AND;
          ALU_SLT: ALU_opcode_o=ALU_SLT;
        endcase
      end

      5'b00_000:begin//Load upper immediate inst
        inst_rd = inst[11:7];
        immediate = inst
      end
      default: ALU_opcode_o=ALU_ADD;
    endcase



  end



endmodule
