module alu#(parameter XLEN=32)(
  input logic [2:0] op1,
  input logic [1:0] op2,
  input logic [XLEN-1:0] op_rs1,
  input logic [XLEN-1:0] op_rs2,
  output [XLEN-1:0] res_rd,
  output res_carry
);

//instruction supported:
  //add(00 000)
  //sub(01 000)
  //sll(00 001) -> shift left logical
  //slt(00 010) -> signed compare rs1,rs2 if rs1<rs2 write 1 else 0
  //sltu(00 011) ->unsigned compare rs1,rs2 if rs1<rs2 write 1 else 0
  //xor(00 100) ->
  //srl(00 101) -> shift right logical
  //sra(01 101) -> shift right arithmetic
  //or(00 110)
  //and(00 111)

  wire [4:0] operation = {op2,op1};
  logic [XLEN-1:0] res;
  logic temp_carry;

  always@(*) begin
    case(operation)
      5'b0: {temp_carry,res}={1'b0,op_rs1}+{1'b0,op_rs2};
      5'b01000: {temp_carry,res}={1'b0,op_rs1}-{1'b0,op_rs2};
      5'b00001: res=op_rs1<<op_rs2;
      5'b00010: begin
        if(op_rs1[XLEN-1]>op_rs2[XLEN-1]) res={31'b0,1'b1};
        else if (op_rs1[XLEN-1]<op_rs2[XLEN-1]) res=32'b0;
        else begin
          if (op_rs1[XLEN-2:0]>op_rs2[XLEN-2:0]) res={31'b0,1'b1};
          else res={31'b0,1'b0};
        end
      end
      5'b00011: begin
          if (op_rs1[XLEN-1:0]>op_rs2[XLEN-1:0]) res={31'b0,1'b1};
          else res=32'b0;
      end
      5'b00100: begin
        res=op_rs1^op_rs2;
      end
      5'b00101:res=op_rs1>>op_rs2;
      5'b01101:res=op_rs1>>>op_rs2;
      5'b00110:res=op_rs1|op_rs2;
      5'b00111:res=op_rs1&op_rs2;
    endcase
  end


  assign res_rd = res;
  assign res_carry=temp_carry;


endmodule
