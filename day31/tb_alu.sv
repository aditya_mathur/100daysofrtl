module tb_alu();

  localparam XLEN=8;
  logic [2:0] op1;
  logic [1:0] op2;
  logic [XLEN-1:0] op_rs1;
  logic [XLEN-1:0] op_rs2;
  wire [XLEN-1:0] res_rd;
  wire res_carry;

  alu#(.XLEN(XLEN)) inst(.*);


  integer i;
  initial begin
    for(i=0;i<100;i=i+1) begin
      op1=$random();
      op2=2'b00;
      op_rs1=$urandom_range(8'h0,8'hf);
      op_rs2=$urandom_range(8'h0,8'hf0);
      #5;
    end
    $finish();
  end

  initial begin
    $dumpvars(0,tb_alu);
    $dumpfile("dump.vcd");
  end

endmodule
