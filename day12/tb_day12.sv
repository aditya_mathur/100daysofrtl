module tb_day12();

  logic clk=1'b0,rst;
  wire [2:0] count;

  syncCounter inst(.*);

  always#5 clk=~clk;

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    rst=1'b1;
    for(i=0;i<16;i=i+1) begin
      @(posedge clk);
    end
    #10
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day12);
    $dumpfile("dump.vcd");
  end

endmodule
