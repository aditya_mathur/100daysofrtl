module syncCounter(
  input clk,rst,
  output logic [2:0] count
);

  logic d1,d2,d3;
  wire q0,q1,q2,q3;

  assign d1=q0^q1;
  assign d2=(q0&q1)^q2;
  assign d3=(q1&q2)^q3;

  DFF inst(.clk(clk),.rst(rst),.d(1'b1),.q(q0));
  DFF inst1(.clk(clk),.rst(rst),.d(d1),.q(q1));
  DFF inst2(.clk(clk),.rst(rst),.d(d2),.q(q2));
  DFF inst3(.clk(clk),.rst(rst),.d(d3),.q(q3));

  assign count={q3,q2,q1};

endmodule


module DFF(
  input clk,rst,d,
  output logic q
);

  always@(posedge clk or negedge rst) begin
    if(!rst) q<=1'b0;
    else q<=d;
  end

endmodule
