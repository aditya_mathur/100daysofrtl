module xnor_day6(out,a,b);
  input a,b;
  output reg out;

  udp_xnor inst(out,a,b);

endmodule

primitive udp_xnor(out,a,b);

  output out;
  input a,b;

table
//a b : out
  0 0 : 1;
  0 1 : 0;
  1 0 : 0;
  1 1 : 1;

endtable

endprimitive
