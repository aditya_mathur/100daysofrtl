module tb_day6();

  reg a,b;
  wire out;

  xnor_day6 inst(.*);

  integer i;
  initial begin
    for(i=0;i<100;i=i+1) begin
      {a,b}=$urandom_range(2'b00,2'b11);
      #5;
    end
    #50
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day6);
    $dumpfile("dump,vcd");
  end

endmodule
