module tb_day24();

  localparam width=8;
  logic [width-1:0] in;
  wire [$clog2(width)-1:0] out;

  priority_encoder inst(.*);
  integer i;
  initial begin
    for(i=0;i<32;i=i+1) begin
      in=$urandom_range(8'h0,8'hff);
      #5;
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day24);
    $dumpfile("dump.vcd");
  end

endmodule
