module priority_encoder#(parameter width=8)(
  input [width-1:0] in,
  output logic [$clog2(width)-1:0] out
);

  always_comb begin
    casex(in)
      8'b1XXXXXXX:out=3'b111;
      8'b01XXXXXX:out=3'b110;
      8'b001XXXXX:out=3'b101;
      8'b0001XXXX:out=3'b100;
      8'b00001XXX:out=3'b011;
      8'b000001XX:out=3'b010;
      8'b0000001X:out=3'b001;
      8'b00000001:out=3'b000;
    endcase
  end

endmodule
