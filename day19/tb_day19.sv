module tb_day19();

  localparam width=4;
  logic [width-1:0] a,b;
  logic cin;

  wire [width-1:0] cout,sum;
  wire [width:0] out;

  ripple_adder inst(.*);

  integer i;
  initial begin
    for(i=0;i<32;i=i+1) begin
      a=$urandom_range(4'b0,4'b1111);
      b=$urandom_range(4'b0,4'b1111);
      cin=$random();
      #15;
    end
    #5
    $finish();
  end
  initial begin
    $dumpvars(0,tb_day19);
    $dumpfile("dump.vcd");
  end

endmodule
