module ripple_adder#(parameter width=4)(
  input [width-1:0] a,b,
  input cin,
  output logic [width-1:0] sum,
  output logic [width-1:0] cout,
  output logic [width:0] out
);

  adder inst1(.a(a[0]),.b(b[0]),.cin(cin),.sum(sum[0]),.cout(cout[0]));

  genvar i;
  generate
    for(i=1;i<width;i=i+1) begin
      adder inst(.a(a[i]),.b(b[i]),.cin(cout[i-1]),.sum(sum[i]),.cout(cout[i]));
    end
  endgenerate

  assign out = {cout[width-1],sum};

endmodule
module adder(
  input a,b,cin,
  output logic sum,cout
);

  always_comb begin
    sum=a^b^cin;
    cout=a&b|cin&(a|b);
  end


endmodule
