module tb_day11();

  logic clk=1'b0,rst;
  logic cntDir=1'b1;
  wire [2:0]q;

  always#5 clk=~clk;

  asyncUpDownCounter inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    rst=1'b1;
    for(i=0;i<16;i=i+1) begin
      @(posedge clk);
    end
    @(posedge clk);
    cntDir=1'b0;
    for(i=0;i<16;i=i+1) begin
      @(posedge clk);
    end
    #10
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day11);
    $dumpfile("dump.vcd");
  end

endmodule
