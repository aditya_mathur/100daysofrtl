module asyncUpDownCounter(
  input clk,rst,cntDir,//counter direction up/down
  output [2:0] q
);

  wire [1:0]q0,q1,q2;
  wire q0ck1,q1ck2;
  wire t=1'b1;

  //cntDir =1-> up counter
  //cntDir =0-> down counter
  assign q0ck1=(cntDir)?q0[1]:q0[0];
  assign q1ck2=(cntDir)?q1[1]:q1[0];

  TFF inst(.clk(clk),.rst(rst),.t(t),.q(q0[0]),.q_bar(q0[1]));
  TFF inst1(.clk(q0ck1),.rst(rst),.t(t),.q(q1[0]),.q_bar(q1[1]));
  TFF inst2(.clk(q1ck2),.rst(rst),.t(t),.q(q2[0]),.q_bar(q2[1]));

  assign q={q2[0],q1[0],q0[0]};

endmodule

module TFF(
  input clk,rst,t,
  output logic q,q_bar
);

  always@(posedge clk or negedge rst) begin
    if(!rst) q<=1'b0;
    else begin
      if(t)q<=~q;
      else q<=q;
    end
  end
  assign q_bar=~q;

endmodule
