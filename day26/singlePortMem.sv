module singlePortMem#(parameter DEPTH=8,WORDSIZE=32)(
  input clk,rst,
  input[$clog2(DEPTH)-1:0] w_addr_i,
  input [WORDSIZE-1:0] w_data_i,
  input w_en_i,
  output logic [WORDSIZE-1:0] r_data_o
);

  logic [WORDSIZE-1:0] mem [DEPTH-1:0];


  initial begin
    integer i;
    for(i=0;i<DEPTH;i=i+1) begin
      mem[i]='b0;
    end
  end

  always@(posedge clk or negedge rst) begin
    integer i;
    if(!rst) begin
      for(i=0;i<DEPTH;i=i+1) begin
        mem[i]='b0;
      end
    end
    else begin
      if(w_en_i) mem[w_addr_i]<=w_data_i;
    end
  end

  always_comb begin
    r_data_o=mem[w_addr_i];
  end

endmodule
