module tb_day26();

  localparam DEPTH=8,WORDSIZE=8;

  wire [WORDSIZE-1:0] r_data_o;
  logic [$clog2(DEPTH)-1:0] w_addr_i;
  logic [WORDSIZE-1:0] w_data_i;
  logic w_en_i;
  logic clk=1'b0,rst=1'b0;

  always#5 clk=~clk;
  singlePortMem#(.DEPTH(DEPTH),.WORDSIZE(WORDSIZE)) inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    rst=1'b1;
    w_en_i=1'b1;
    for(i=0;i<DEPTH;i=i+1) begin
      w_addr_i=i;
      w_data_i=$urandom_range(8'b0,8'hff);
      @(posedge clk);
    end

    //read cycle
    w_en_i=1'b0;
    for(i=0;i<=DEPTH;i=i+1) begin
      w_addr_i=$urandom_range(3'b0,3'b111);
      @(posedge clk);
    end
    #50
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day26);
    $dumpfile("dump.vcd");
  end




endmodule
