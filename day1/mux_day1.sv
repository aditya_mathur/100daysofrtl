module mux_day1(
  input [7:0] a,b,
  input sel,
  output [7:0] out
);

  assign out = (sel)?a:b;//if sel=1 out=a else b

endmodule
