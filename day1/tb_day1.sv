module tb_day1();

  logic [7:0]a,b;
  logic sel;

  wire [7:0] out;

  mux_day1 inst1(.*);//instantiation

  integer i;

  initial begin
    for(i=0;i<100;i=i+1) begin
      a=$urandom_range(0,'hff);
      b=$urandom_range(0,'hff);
      sel=$urandom_range(0,1'b1);
      #5;//delay
    end
    #200
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day1);
    $dumpfile("dump.vcd");
  end

endmodule
