module tb_day18();

  logic clk=1'b0,rst;
  logic d;
  wire q;

  synchronizer inst(.*);


  always#5 clk=~clk;

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    @(posedge clk);
    rst=1'b1;
    for(i=0;i<32;i=i+1) begin
      d=$random();
      @(posedge clk);
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day18);
    $dumpfile("dump.vcd");
  end

endmodule
