module synchronizer(
  input clk,rst,
  input d,
  output logic q
);

  //2 depth synchr
  wire d1;
  DFF inst1(.clk(clk),.rst(rst),.q(d1),.d(d));
  DFF inst2(.clk(clk),.rst(rst),.d(d1),.q(q));

endmodule


module DFF(
  input d,
  input clk,rst,
  output logic q
);

  always@(posedge clk or negedge rst) begin
    if(!rst) q<=1'b0;
    else q<=d;
  end

endmodule
