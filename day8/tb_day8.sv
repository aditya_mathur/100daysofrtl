module tb_day8();

  logic clk=1'b0,rst,serial_in;
  wire[3:0] serial_out;


  shift_reg_day8 inst(.*);

  always#5 clk=~clk;

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    @(posedge clk);
    rst=1'b1;
    serial_in<=1'b1;
    @(posedge clk);
    serial_in<=1'b0;
    @(posedge clk);
    serial_in<=1'b0;
    for(i=0;i<100;i=i+1) begin
      serial_in<=$random%2;
      #5;
    end
    #50
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day8);
    $dumpfile("dump.vcd");
  end


endmodule
