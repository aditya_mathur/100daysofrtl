module shift_reg_day8(
  input clk,rst,serial_in,
  output [3:0] serial_out
);

  logic [3:0] p_state;
  logic [3:0] nxt_state;

  always@(posedge clk or negedge rst) begin
    if(!rst)
      p_state<=4'h0;
    else begin
      p_state<=nxt_state;
    end
  end

  assign nxt_state={p_state[2:0],serial_in};
  assign serial_out = nxt_state;

endmodule
