module nor_day5(
  input a,b,
  output out,out1,
  output logic out2
);


  //gate level modeling
  //
  wire w1,w2;

  not ins1(w1,a);
  not ins2(w2,b);

  and ins3(out,w1,w2);

  //dataflow modeling
  //
  assign out1=~a&~b;

  //behavioural modeling
  //
  always_comb begin
    case({a,b})
      2'b00:out2=1'b1;
      2'b01:out2=1'b0;
      2'b10:out2=1'b0;
      2'b11:out2=1'b0;
      default: out2=1'b0;
    endcase
  end


endmodule
