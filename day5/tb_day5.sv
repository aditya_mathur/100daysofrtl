module tb_day5();

  logic a,b;
  wire out,out1,out2;

  nor_day5 inst(.*);

  integer i;
  initial begin
    for(i=0;i<100;i=i+1) begin
      {a,b}=$urandom_range(2'b00,2'b11);
      #5;
    end
    #50
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day5);
    $dumpfile("dump.vcd");
  end
endmodule
