module tb_day23();

  localparam width=4;
  logic [width-1:0] in;
  logic op;

  wire [width-1:0] out;

  binary2gray inst(.*);

  integer i;
  initial begin
    for(i=0;i<16;i=i+1) begin
      {op,in}=$urandom_range(5'b0,5'b11111);
      #5;
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day23);
    $dumpfile("dump.vcd");
  end


endmodule
