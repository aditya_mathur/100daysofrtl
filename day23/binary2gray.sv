module binary2gray#(parameter width=4)(
  input [width-1:0] in,
  input op,//select operation b2g or g2b
  output [width-1:0] out
);

  logic [width-1:0] in_shift;
  logic [width-1:0] binary_out, gray_out;
  always_comb begin
    in_shift = in >> 1'b1;
    gray_out= in^in_shift;//binary to gray
  end

  genvar i;
  generate
    assign binary_out[width-1]=in[width-1];
    for (i=width-2;i>=0;i=i-1) begin
     assign  binary_out[i]=in[i]^binary_out[i+1];
    end
  endgenerate

  assign out = (op)?binary_out:gray_out;

endmodule
