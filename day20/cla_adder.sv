module cla_adder#(parameter width=4)(
  input [width-1:0] a,b,
  input cin,
  output logic [width:0] sum
);

  wire [width-1:0] g_bit,p_bit;
  wire [width:0] c;
  wire [width-1:0] cout,sum_internal;
  genvar i;
  //generation bit
  generate
    for(i=0;i<width;i=i+1) begin
      assign g_bit[i]=a[i]&b[i];
      assign p_bit[i]=a[i]|b[i];
      assign c[i+1]=g_bit[i]|c[i]&p_bit[i];
    end
  endgenerate

  assign c[0]=cin;

  //instantiation of adder
  genvar j;
  generate
    for(j=0;j<width;j=j+1) begin
      adder inst(.a(a[j]),.b(b[j]),.cin(c[j]),.sum(sum_internal[j]),.cout(cout[j]));
    end
  endgenerate

  assign sum={c[width],sum_internal};

endmodule


module adder(
  input a,b,cin,
  output logic sum,cout
);

  always_comb begin
    sum=a^b^cin;
    cout=a&b|cin&(a|b);
  end

endmodule
