module tb_day20();

  localparam width=4;
  logic [width-1:0] a,b;
  logic cin;
  wire [width:0] sum;
  cla_adder inst(.*);

  integer i;
  initial begin
    for(i=0;i<32;i=i+1) begin
      a=$urandom_range(4'b0,4'b1111);
      b=$urandom_range(4'b0,4'b1111);
      cin=$random();
      #5;
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day20);
    $dumpfile("dump.vcd");
  end

endmodule
