module tb_day25();

  localparam WIDTH=4;
  logic signed [WIDTH-1:0] multiplier,multiplicand ;
  logic clk=1'b0,rst;
  wire signed [2*WIDTH-1:0] product;
  wire done;

  always#5 clk=~clk;

  boothMultiplier#(.WIDTH(WIDTH)) inst(.*);


  integer i;

  initial begin
    @(posedge clk);
    rst=1'b0;
    @(posedge clk);
    @(posedge clk);
    rst=1'b1;
    multiplicand=$urandom_range(4'b0,4'b0111);
    multiplier=$urandom_range(4'b0,4'b0111);
    @(posedge clk);
    for(i=0;i<320;i=i+1) begin
      if(done) begin
        multiplicand=$urandom_range(4'b0,4'b0111);
        multiplier=$urandom_range(4'b0,4'b0111);
      end
      @(posedge clk);
    end
    #5;
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day25);
    $dumpfile("dump.vcd");
  end

endmodule
