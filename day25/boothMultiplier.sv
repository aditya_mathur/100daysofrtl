module boothMultiplier#(parameter WIDTH=4)(
  input clk,rst,
  input signed [WIDTH-1:0] multiplicand,multiplier,
  output logic signed [2*WIDTH-1:0] product,
  output logic done
);

  parameter [3:0] START=4'b0000,OPR=4'b0001,SHIFT=4'b0010,RESULT=4'b0100;
  logic [WIDTH-1:0] nxtState,preState;
  logic [WIDTH-1:0] AC,QR,BR;

  logic Qnp1=1'b0;
  integer count=WIDTH-1;

  always@(posedge clk or negedge rst) begin
    if(!rst) begin
      preState<=START;
      {AC,BR,QR}='b0;
    end
    else preState<=nxtState;
  end

  always@(preState or multiplier or multiplicand) begin
    done=1'b0;
    nxtState='bx;
    case(preState)
      START: begin
        BR=multiplicand;
        QR=multiplier;
        count=WIDTH-1;
        AC=4'b0;
        Qnp1=1'b0;
        done=1'b0;
        if({QR[0],Qnp1}==2'b01 || {QR[0],Qnp1}==2'b10) nxtState=OPR;
        else nxtState=SHIFT;
        $display("**Present State:START; NextState:%b -> AC:%b,BR:%b,QR:%b,Qnp1:%b",nxtState,AC,BR,QR,Qnp1);
      end
      OPR: begin
        if({QR[0],Qnp1}==2'b01) begin
          AC<=AC+BR;
        end
        else if({QR[0],Qnp1}==2'b10)
        begin
          AC<=AC+~(BR)+1'b1;
        end
        nxtState=SHIFT;
        $display("preState:%b, NextState:%b -> AC:%b,BR:%b,QR:%b,Qnp1:%b",preState,nxtState,AC,BR,QR,Qnp1);
      end
      SHIFT:begin
        AC<={AC[WIDTH-1],AC[WIDTH-1:1]};
        QR<={AC[0],QR[WIDTH-1:1]};
        Qnp1<=QR[0];
        if(count!=0) begin
          count<=count-1;
          nxtState=OPR;
        end
        else begin
          nxtState=RESULT;
        end
        $display("preState:%b, NextState:%b -> AC:%b,BR:%b,QR:%b,Qnp1:%b",preState,nxtState,AC,BR,QR,Qnp1);
      end
      RESULT:begin
        done=1'b1;
        nxtState=START;
        $display("preState:RESULT, NextState:%b -> AC:%b,BR:%b,QR:%b,Qnp1:%b,DONE:%b",nxtState,AC,BR,QR,Qnp1,done);
      end
    endcase
  end

  always_comb begin
    if(done) product={AC,QR};
  end


endmodule
