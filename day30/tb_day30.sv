module tb_day30();

  logic clk=1'b0,rst,din;
  wire dout;

  fallEdgeextender inst(.*);

  always#5 clk=~clk;

  integer i;
  initial begin
    rst=1'b0;
    din=1'b0;
    @(posedge clk);
    rst=1'b1;
    @(posedge clk);
    for(i=0;i<50;i=i+1) begin
      din=$random();
      @(posedge clk);
      @(posedge clk);
      @(posedge clk);
      @(posedge clk);
    end
    #5;
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day30);
    $dumpfile("dump.vcd");
  end

endmodule
