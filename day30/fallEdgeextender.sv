module fallEdgeextender(
  input clk,rst,
  input din,
  output logic dout
);

  logic [1:0] pState,nState;

  always@(posedge clk or negedge rst) begin
    if (!rst) begin
      dout=1'b0;
      pState=1'b0;
    end
    else begin
      pState=nState;
    end
  end


  always@(pState or din) begin
    case(pState)
      2'b00: begin
        if(din==1'b0) begin
          nState=2'b00;
          dout=1'b0;
        end
        else begin
          nState=2'b01;
          dout=1'b1;
        end
      end
      2'b01: begin
        if(din==1'b1) nState=2'b01;
        else nState=2'b10;
      end
      2'b10: begin
        if(din==1'b1) nState=2'b11;
        else nState=2'b11;
      end
      2'b11:begin
        nState=2'b00;
        dout=1'b0;
      end
    endcase
  end

endmodule
