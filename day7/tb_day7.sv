module tb_day7();

  logic t,clk=1'b0,rst;

  wire q;

  always#5 clk=~clk;
  t_FF_day7 inst1(.*);

  integer i;
  initial begin
    rst<=1'b0;
    #15;
    rst=1'b1;
    for(i=0;i<=20;i=i+1) begin
      t=$urandom_range(1'b0,1'b1);
      #5;
    end
    #50
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day7);
    $dumpfile("dump.vcd");
  end

endmodule
