module t_FF_day7(
  input t,clk,rst,
  output logic q
);

  //behavioural modeling

  always@(posedge clk) begin
    if(!rst) q=1'b0;
    else begin
      if(t) q<=~q;
      else q<=q;
    end
  end

endmodule
