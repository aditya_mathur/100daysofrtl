module easy_barrel_shifter#(parameter width=4)(
  input [width-1:0] in,
  input [$clog2(width)-1:0] shift,
  output logic [width-1:0] out
);

  localparam doubleWidth = width*2;

  wire [doubleWidth-1:0] double_in= {in,in};

  assign out=double_in >> shift;


endmodule
