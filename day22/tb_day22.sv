module tb_day22();

  localparam width=4;
  logic [width-1:0] in;
  logic [$clog2(width)-1:0] shift;
  wire[width-1:0] out;

  easy_barrel_shifter#(.width(width)) inst1(.*);

  integer i;
  initial begin
    for(i=0;i<32;i=i+1) begin
      in=$urandom_range(4'b0,4'hf);
      shift=$urandom_range(2'b0,2'b11);
      #5;
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day22);
    $dumpfile("dump.vcd");
  end
endmodule
