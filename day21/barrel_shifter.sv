module barrel_shifter#(parameter width=4)(
  input [width-1:0] in,
  input [1:0] sel,
  output logic [width-1:0] mux_out
);

  wire [width-1:0] input0,input1,input2,input3;

  assign input0={in[1],in[2],in[3],in[0]};
  assign input1={in[2],in[3],in[0],in[1]};
  assign input2={in[3],in[0],in[1],in[2]};
  assign input3={in[0],in[1],in[2],in[3]};

  MUX#(.width(4)) inst0(.data_in(input0),.sel(sel),.data_out(mux_out[0]));

  MUX#(.width(4)) inst1(.data_in(input1),.sel(sel),.data_out(mux_out[1]));

  MUX#(.width(4)) inst2(.data_in(input2),.sel(sel),.data_out(mux_out[2]));

  MUX#(.width(4)) inst3(.data_in(input3),.sel(sel),.data_out(mux_out[3]));


endmodule

module MUX#(parameter width=4)(
  input [width-1:0] data_in,
  input [1:0]sel,
  output logic  data_out
);

  always@(*) begin
    case(sel)
      2'b00:data_out=data_in[0];
      2'b01:data_out=data_in[1];
      2'b10:data_out=data_in[2];
      2'b11:data_out=data_in[3];
    endcase
  end

endmodule
