module tb_day21();

  localparam width=4;
  logic [width-1:0] in;
  logic [$clog2(width)-1:0] sel;
  wire [width-1:0] mux_out;

  barrel_shifter inst1(.*);

  integer i;
  initial begin
    for(i=0;i<=16;i=i+1) begin
      in=$urandom_range(4'b0,4'hf);
      sel = $urandom_range(2'b0,2'b11);
      #5;
    end
    #5
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day21);
    $dumpfile("dump.vcd");
  end


endmodule
