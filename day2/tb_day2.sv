module tb_day2();

  reg [1:0] d;
  reg clk=1'b0,rst;
  wire [1:0] q1,q2,q3;


  ff_day2 inst(.*);

  integer i;

  always#5 clk=~clk;

  initial begin
    rst=1'b0;
    #10;
    rst=1'b1;
    for(i=0;i<10;i=i+1) begin
      d<=$urandom_range(0,2'b11);
      #5;
    end
    for(i=0;i<10;i=i+1) begin
      d<=$urandom_range(0,2'b11);
      #5;
      rst<=(d[0])?1'b1:1'b0;
    end
    #50;
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day2);
    $dumpfile("dump.vcd");
  end

endmodule
