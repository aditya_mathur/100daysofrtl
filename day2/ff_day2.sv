module ff_day2(
  input [1:0] d,
  input clk,rst,
  output reg[1:0] q1,q2,q3
);

  always@(posedge clk) begin
    if(!rst) q1<=1'b0;
    else q1<=d;
  end


  always@(posedge clk or negedge rst) begin
    if(!rst) q2<=1'b0;
    else q2<=d;
  end

  always@(posedge clk) begin
    q3<=d;
  end

endmodule
