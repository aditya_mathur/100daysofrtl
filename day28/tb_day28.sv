module tb_day28();

  localparam WIDTH=4;
  logic [WIDTH-1:0] divisor,dividend;
  logic clk=0,rst;
  wire [WIDTH-1:0] reminder,quotient;
  wire done;

  always#5 clk=~clk;

  nonRestoringDiv#(.WIDTH(WIDTH)) inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    @(posedge clk);
    rst=1'b1;
    divisor=$urandom_range(4'b0,4'b1111);
    dividend=$urandom_range(4'b0,4'b1111);
    for(i=0;i<200;i=i+1) begin
      if(done) begin
        divisor=$urandom_range(4'b0,4'b0011);
        dividend=$urandom_range(4'b0,4'b1111);
        @(posedge clk);
      end
      else @(posedge clk);
    end
    #100;
    @(posedge clk);
    $finish();
  end

  initial begin
    $dumpvars(0,tb_day28);
    $dumpfile("dump.vcd");
  end

endmodule
