module nonRestoringDiv#(parameter WIDTH=8)(
  input clk, rst,
  input [WIDTH-1:0] divisor,
  input [WIDTH-1:0] dividend,
  output logic [WIDTH-1:0] quotient,
  output logic [WIDTH-1:0] reminder,
  output logic done
);

  parameter [2:0] start=3'b000,shift_update_acc=3'b001,updated_q=3'b011,result=3'b010;

  logic [2:0] pState,nState;
  logic [WIDTH-1:0] quo,acc,m,m_comp;
  integer count;

  always@(posedge clk or negedge rst) begin
    if(!rst) begin
        pState=start;
        done=1'b0;
    end
    else pState<=nState;
  end

  always@(pState or divisor or dividend)begin
    case(pState)
      start: begin
        count=WIDTH;
        nState='bx;
        quo=dividend;
        acc='b0;
        m=divisor;
        m_comp=~m+1'b1;
        nState=shift_update_acc;
        done=1'b0;
      end
      shift_update_acc:begin
        //shifting
        //acc<-quo
        acc={acc[WIDTH-2:0],quo[WIDTH-1]};
        quo={quo[WIDTH-2:0],1'bx};
        //checking the sign of Acc
        if(acc[WIDTH-1]) begin
          acc=acc+m;
        end
        else begin
          acc=acc+m_comp;
        end
        nState=updated_q;
      end
      updated_q:begin
        if(acc[WIDTH-1]) quo[0]=1'b0;
        else quo[0]=1'b1;
        count=count-1;
        if(count>0) nState=shift_update_acc;
        else nState=result;
      end
      result:begin
        if(acc[WIDTH-1]) acc=acc+m;
        reminder=acc;
        quotient=quo;
        done=1'b1;
        nState=start;
      end
    endcase
  end

endmodule
