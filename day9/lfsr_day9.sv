module lfsr_day9(
  input rst,clk,
  output [3:0] out
);

  logic [3:0] lsfr_ff;
  logic [3:0] nxt_ff;

  always@(posedge clk or negedge rst) begin
    if(!rst) lsfr_ff<=4'b1110;
    else lsfr_ff<=nxt_ff;
  end

  assign nxt_ff = {lsfr_ff[2:0],lsfr_ff[3]^lsfr_ff[0]};

  assign out = lsfr_ff;

endmodule
