module tb_day9();

  logic clk=1'b0,rst;
  wire [3:0] out;

  always#5 clk=~clk;

  lfsr_day9 inst(.*);

  integer i;
  initial begin
    rst=1'b0;
    @(posedge clk);
    rst=1'b1;
    for(i=0;i<16;i=i+1) begin
      @(posedge clk);
    end

    #20
    $finish();

  end

  initial begin
    $dumpvars(0,tb_day9);
    $dumpfile("dump.vcd");
  end
endmodule
