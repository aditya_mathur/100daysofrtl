module day4_nand(
  input  a,b,
  output out,out2,
  output logic out1
);

  //gate based modeling
  //
  wire w1,w2;

  not ins1(w1,a);
  not ins2(w2,b);

  or ins3(out,w1,w2);


  //dataflow modeling
  //
  assign out2 = ~a|~b;

  //behavioural modeling
  //
  always_comb begin
    case({a,b})
      2'b00:out1=1'b1;
      2'b01:out1=1'b1;
      2'b10:out1=1'b1;
      2'b11:out1=1'b0;
      default:out1=1'b0;
    endcase
  end


endmodule
